.. _techguide-localization-index:localization:

Localization
============

Translations are stored module-wise in .po files located in directories called *locale*.
Trytons builtin features are described here:

https://docs.tryton.org/6.0/server/topics/translation.html

However we use Weblate to manage the translation:

https://weblate.org/en/

.. _techguide-localization-index:localization-translating_gnu_health:

Translating GNU Health
----------------------

The language of the GNU Health user interface is controlled through gettext language packs.

Language packs are are managed, stored and distributed through an online translation platform. This makes maintaining translations easier and faster, and it makes your GNU Health installation leaner, since it contains only the languages needed by your health institution.

The official GNU Health translation portal is now hosted at `Codeberg <https://translate.codeberg.org/projects/gnuhealth/>`_.

There is a mailing list for all translation related discussions at https://lists.gnu.org/mailman/listinfo/health-i18n . If you are a GNU Health translator, you should subscribe to this mailing list.

When translating GNU Health to your language, please start from the *health* module which contains the core of GNU Health.

.. _techguide-localization-index:localization-installation_of_language_packs:

Installation of Language Packs
------------------------------
We will go through an example on how to enable the Spanish language and to install the language pack. For this example, we will work on the demo database :code:`gnuhealth_demo_generic`.

* **Step 1:** Declare your language and make it translatable. This can be done in the *Administration → Localization → Languages* section.

Remember the code of the language (in this case **es**).

* **Step 2:** Download and uncompress the language pack file for your language and for the specific resource from the translation server.

With the GNU Health administrator user (*gnuhealth*) execute the following commands.

.. note::
    Substitute the language sample code (es) by your language

.. warning::
    The language files that are retrieved come from development translation server! The official version and languages are shipped in the standard installation. Always try in a development environment before use in production.This overwrites the existing translation files !

.. code-block:: console


    $ gnuhealth-control getlang es


This installs *all the modules* translation files for the Spanish Spain (es) language.

* **Step 3:** Change your user preferences to your new language:
* **Step 4:** Log out and stop your GNU Health Tryton server (if it's running).
* **Step 5:** Update your GNU Health instance with the command

.. code-block:: console


    $ trytond-admin --all --database=gnuhealth_demo_generic


Sometime, the about command may not work well as expected, user can try below way:

1. Open language form: Administration > Localization > Languages > <YOUR-LANG>
2. Click 'Unload translations' then click 'Load translations'"


* **Step 6:** Start your sever with the new language for your module installed.

.. _techguide-localization-index:localization-setting_the_user_language:

Setting the User Language
-------------------------

Please remember that the language is a user preference. In the same database, you can have different users using different languages.
Read the :ref:`userguide-healthcenter-accessmanagement:access_management-users-preferences_tab` chapter to see how.



.. _techguide-localization-index:localization-update_po_and_pot_files:

Update po and pot files
-----------------------

**NOTE: This section is mainly used by GNU Health HIS developers.**

GNU Health have a tool named *po-export* in scripts directory, it can
be used to update po and pot files, this tool is used localization
feature of tryton.

.. code-block:: console


    $ cd tryton/scripts/po-export
    $ bash po-export.sh <LANG>
    $ bash po-export.sh --all


When HIS code is updated, for example: labels and helps of fields,
report templates or data files, pot and po files should be updated
too.

Before run po-export.sh, the following steps is suggested, for they
can reduce po files merge conflicts.

1. Open GNU Health weblate page: (https://translate.codeberg.org/projects/gnuhealth/)
2. Lock translation: *(Manage > Repository maintenance > Lock)*
3. Commit pending changes: *(Manage > Repository maintenance > (pending changes) Commit)*
4. Push outgoing commits: *(Manage > Repository maintenance > Push)*
5. Pull and update changes from gnuhealth upstream repo to your working repo.

After run po-export.sh successfully:

1. Push po files changes to GNU Health upstream repo.
2. Open GNU Health weblate page: (https://translate.codeberg.org/projects/gnuhealth/)
3. Sync all changes to weblate: *(Manage > Repository maintenance > (missing commits) Update)*
4. Unlock translation: *(Manage > Repository maintenance > Unlock)*
