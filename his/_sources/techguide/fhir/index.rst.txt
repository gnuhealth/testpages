.. _techguide-fhir-index:fhir_rest:

FHIR REST
=========

.. warning:: This is still in development and does not fulfill the FHIR specification!

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   usage
