.. _userguide-modules-nursing:nursing:

Nursing
=======

.. _userguide-modules-nursing:nursing-introduction_to_nursing:

Introduction to Nursing
-----------------------

The :code:`health_nursing` module provides functionality to document *Roundings* (for inpatients) and *Amulatory Care* (for outpatients) alike. This functionality can be found in the *Health → Nursing* section of GNU Health.

.. _userguide-modules-nursing:nursing-roundings:

Roundings
---------

.. _userguide-modules-nursing:nursing-roundings-*main*_tab:

*Main* Tab
^^^^^^^^^^

.. _userguide-modules-nursing:nursing-roundings-*icu*_tab:

*ICU* Tab
^^^^^^^^^

.. _userguide-modules-nursing:nursing-roundings-*procedures*_tab:

*Procedures* Tab
^^^^^^^^^^^^^^^^

.. _userguide-modules-nursing:nursing-roundings-*medication*_tab:

*Medication* Tab
^^^^^^^^^^^^^^^^

.. _userguide-modules-nursing:nursing-roundings-*stock_moves*_tab:

*Stock Moves* Tab
^^^^^^^^^^^^^^^^^

.. _userguide-modules-nursing:nursing-ambulatory_care:

Ambulatory Care
---------------

.. _userguide-modules-nursing:nursing-ambulatory_care-*main*_tab:

*Main* Tab
^^^^^^^^^^

The *Main* tab provides some basic information plus the procedures to be performed by the nurses during ambulatory care. The following fields are available:

* *ID*
* *Health Professional*
* *Ordering Physician*
* *Patient*
* *Base Condition*
* *Related Evaluation*
* *Session*
* *Start*: Date and time
* *Procedures*, each consisting of a *Code* and *Comments*
* *Summary*
* *Warning*
* *End*: Date and time
* *Next Session*: Date and time
* *State*

.. _userguide-modules-nursing:nursing-ambulatory_care-*other*_tab:

*Other* Tab
^^^^^^^^^^^

The *Other* tab allows you to store some very basic parameters regarding the patients overall condition. The following fields are available:

* *Temperature*
* *Systolic Pressure* and *Diastolic Pressure*
* *Heart Rate*
* *Respiratory Rate*
* *Oxygen Saturation*
* *Glycemia*
* *Evolution*: Choose from "Initial", "Status quo", "Improving", or "Worsening".

.. _userguide-modules-nursing:nursing-ambulatory_care-*medication*_tab:

*Medication* Tab
^^^^^^^^^^^^^^^^

The *Medication* tab for ambulatory care is very much the same like the *Medication* tab for roundings. The only difference is that the location (in terms of stock management) is labelled *Care Location* (instead of *Hospitalization Location*).

.. _userguide-modules-nursing:nursing-ambulatory_care-*stock_moves*_tab:

*Stock Moves* Tab
^^^^^^^^^^^^^^^^^

The *Stock Moves* tab for ambulatory care is identical to the *Stock Moves* tab for roundings.
