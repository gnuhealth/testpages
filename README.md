# GNU Health Documentation

Welcome to the GNU Health Documentation!

## Source and Build repositories

We are gathering the source for the documentations of all subprojects here:

https://codeberg.org/gnuhealth/documentation

Additionally this repository contains a top level index.html with links to the subprojects.

Currently the source of the test branch gets deployed automatically into this repository:

https://codeberg.org/gnuhealth/testpages

The result can be viewed here:

https://gnuhealth.codeberg.page/testpages/

The main branch gets deployed into the top level pages repository.
This is used by our official documentation on https://docs.gnuhealth.org:

https://codeberg.org/gnuhealth/pages

https://gnuhealth.codeberg.page/

Finally we also have a future branch to document upcoming functionality and that one also gets deployed:

https://codeberg.org/gnuhealth/futurepages

https://gnuhealth.codeberg.page/futurepages/

## Editing the documentation

### From Browser

The new approach allows to edit files from browser and see the results without building the documentation locally. This makes it much easier to contribute to the documentation for people without technical background.

You need to log in to Codeberg and get access permissions for the source repository. You can even log in using GitLab or GitHub.

### Building locally

For major changes it is recommended to edit and rebuild locally before committing.
For the Hospital Information System (HIS) you can find instructions how to build the documentation and conventions to follow here:

https://gnuhealth.codeberg.page/testpages/his/techguide/contributing.html#writing-the-documentation

## How to setup the automatic builds?

First of all we had to ask for CI resources:

https://codeberg.org/Codeberg-e.V./requests/issues/195

Having them granted we could enable the source repository on https://ci.codeberg.org.

Codeberg Pages serves HTML files of our main branch of our global pages repository on https://codeberg.org/gnuhealth/pages.
Besides any other repository can have a branch pages that will be accessible on https://codeberg.org/gnuhealth/pages/<repository_name>.

Inside the source repository we define a pipeline in a file called `.woodpecker.yaml`.
This gets triggered on every commit. It creates a Docker containers for every defined step. That's where all the magic happens:
We run the build commands and set up the desired directory structure.
Depending on the branch we push the results in the desired build directory.

To be able to push we use a deploy key. For this we first have to create a local SSH key pair.
The public key is placed on the target build directories as deploy key (inside the repository click Settings -> Deploy Keys -> Add Deploy Key).
The private key is placed on the enabled repository section on ci.codeberg.org (Click on enabled repo -> Settings -> Secrets -> Add Secret). Make sure not to destroy the formatting and to have a blank line in the end when pasting.
Besides the name of the secret has to match the usage in `woodpecker.yaml`.
