.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

Testing
=======

.. warning::
   The following examples are **not designed for productive use**.

Instead they aim to easily set up systems for testing & developing.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   gnuhealth_server_and_client
   gnuhealth_client
   one_system_multiple_applications
   one_application_multiple_systems
