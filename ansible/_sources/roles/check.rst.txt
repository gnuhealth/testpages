.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

check
=====

In the beginning of every deployment this role is executed to ensure some basic requirements.

First of all a SSH connection and a sudo password are needed. The easiest way is to simply set a username and password
in vault. For a more secure setup read the sections `Securing SSH & PWs` and `Encryption by Ansible-Vault`.

Then the operating system will be checked.
If it's not part of the supported operating system families the execution will be aborted.
If it is but not a tested distribution & version you have to accept a warning.
This interactive warning can be disabled using the `ch_ignore_os_warning` boolean.

Besides the cache of the package manager will be updated to ensure it is updated exactly one time.
Since the generic package module does not offer updating the cache this also allows to use the generic module later on for all operating systems.
Furthermore this prevents updating the cache every time a package has to be installed.

- **Variables**:

  - ch_operating_system_families: Supported operating system families
  - ch_operating_system_versions: Supported distribution versions
  - ch_ignore_os_warning: Ignore interactive warning if distribution version is not supported
