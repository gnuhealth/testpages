.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

mygnuhealth
===========

This role installs the mobile application MyGNUHealth on a GNU/Linux system using PyPI / pip.

If using openSUSE or FreeBSD Python 3.11 will be installed and used for it in order not to use (soonish) outdated Python versions.

MyGNUhealth is installed in a virtual environment and the is binary linked to /usr/local/bin/ to have it in the default path.

A desktop entry is created in the applications menu so you can also start it without using the terminal.

- **Parameters**:

  - mygh_version: Version to install if using PyPI (on Debian)

- **Variables**:

  - mygh_packages: Dict of system packages to install before pip installation, taking the OS family as key
  - mygh_venv: Path of virtual python environment
  - mygh_venv_python: Python version to use for virtual environment, differentiated by OS
  - mygh_use_testpypi: If true use test.pypi.org, meant for testing before publishing to pypi.org
  - mygh_bash: Path of bash depending on OS (differs for FreeBSD)
  - mygh_icon: Path of the MyGNUHealth icon for a desktop entry, taken from PyPI package after installation by default
